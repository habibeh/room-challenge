<?php


require_once "vendor/autoload.php";

use App\Database\Connection;
use App\Database\Database;


function create_users_table(Database $db)
{
    $sql = "CREATE TABLE IF NOT EXISTS users (
                          id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                          firstname VARCHAR(50) NOT NULL,
                          lastname VARCHAR(50) NOT NULL,
                          email VARCHAR(50),
                          reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
)";

    $db->execute($sql);
}

function create_carts_table(Database $db)
{
    $sql = "CREATE TABLE IF NOT EXISTS carts (
                          id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                          user_id INT UNSIGNED NOT NULL,
                          total_price DECIMAL(10, 2) default 0,
                          closed BOOLEAN default false,
                          created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                          updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                          FOREIGN KEY (user_id) REFERENCES users(id)
    )";

    $db->execute($sql);
}


function create_cart_items_table(Database $db)
{
    $sql = "CREATE TABLE IF NOT EXISTS cart_items (
                          id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                          cart_id INT UNSIGNED NOT NULL,
                          item_id INT UNSIGNED NOT NULL,
                          quantity INT NOT NULL,
                          checked BOOLEAN default false,
                          created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                          updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                          FOREIGN KEY (cart_id) REFERENCES carts(id),
                          FOREIGN KEY (item_id) REFERENCES items(id)
    )";

    $db->execute($sql);
}

function create_items_table(Database $db)
{
    $sql = "CREATE TABLE IF NOT EXISTS items (
                          id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                          title VARCHAR(255) NOT NULL,
                          slug VARCHAR(255) NOT NULL,
                          description TEXT,
                          price DECIMAL(10, 2) default 0,
                          inventory_count INT default 0,
                          created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                          updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
    )";

    $db->execute($sql);
}


$db = new Database();

create_users_table($db);
create_items_table($db);
create_carts_table($db);
create_cart_items_table($db);

Connection::close();
