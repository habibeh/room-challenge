<?php

require_once "app/bootstrap.php";

error_reporting(E_ERROR | E_PARSE);

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->safeLoad();

require_once "routes/web.php";

\App\Router\Route::serve();

\App\Database\Connection::close();
