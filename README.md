# Shop Application

A simple shopping application able to:

Address: http://95.179.149.51:8001

- [Create Cart](#create-cart)
- [Add Item](#add-item)
- [Edit Item](#edit-item)
- [Delete Item](#delete-item)
- [Fetch Cart](#fetch-cart)
- [Mark as Checked](#mark-as-checked)

## How to Start

1. Then run the application via  `docker-compose`

```
docker-compose up --build -d  

```

3. Install the packages using composer:

```  
docker-compose exec app rm -rf vendor composer.lock  
docker-compose exec app composer install  
```  

2. migrate tables

```
docker-compose exec app composer migrate
```

2. seed user and some items

```
docker-compose exec app composer seed
```

It is ready and served on port  `8001`

```
http://IP_ADDRESS:8001  

```

## API Reference

> Note that because of the simplicity authenticating and user checking mechanism is ignored and just creating order without validating the user  is considerd (`user_id: 1` for this test is received in every request instead of JWT tokens)

### Create Cart
Initialize the cart with for the user with an array of items

**POST** : `/cart`

| Parameter | Type |Description|
|--|--|----|
| user_id | int |the id of user (use `1` for test)|
| items | array |array of items|

*example request:*
```
{
    "user_id": 1,
    "items" : [
        {
            "item_id": 1,
            "quantity": 2
        }
    ]
}
```

**success response:**
```
# status: 200
{
    "cart_id": "6",
    "message": "Cart created."
}
```


### Fetch Cart
Fetch the created cart with it items

**GET** : `/cart`

| Parameter | Type |Description|
|--|--|----|
| id | int |id of the cart |

*example request:*
```
{
	"id": 1
}
```

**success response:**
```
# status: 200
{
	"result": {
		"id": 1,
		"items": [
			{
				"id": 1,
				"cart_id": 1,
				"item_id": 1,
				"quantity": 8,
				"checked": 1,
				"created_at": "2023-10-25 10:26:20",
				"updated_at": "2023-10-25 10:26:29"
			},
			{
				"id": 2,
				"cart_id": 1,
				"item_id": 2,
				"quantity": 4,
				"checked": 0,
				"created_at": "2023-10-25 10:26:20",
				"updated_at": "2023-10-25 10:26:20"
			}
		]
	}
}
```


### Add Item
Add item to an existing cart

**POST** : `/cart/item`

| Parameter | Type |Description|
|--|--|----|
| cart_id | int |the id of cart|
| user_id | int |the id of user (use `1` for test)|
| item_id | int | id of the item|
| quantity | int | number of requested quantity|

example request
```
{
    "user_id": 1,
    "cart_id": 1,
    "item_id": 1,
    "quantity": 10
}
```

**success response:**
```
# status: 200
{
    "result": {
        "cart_item_id": "11"
    },
    "message": "Cart item added."
}
```

### Edit Item
Edit a cart item from an existing cart

**PUT** : `/cart/item`

| Parameter | Type |Description|
|--|--|----|
| id | int | id of the cart-item|
| user_id | int |the id of user (use `1` for test)|
| quantity | int | number of requested quantity|

example request
```
{
    "id": 1,
    "user_id": 1,
    "quantity": 5
}
```

**success response:**
```
# status: 200
{
    "result": {
        "cart_item_id": 6,
        "updated_quantity": 5
    },
    "message": "Cart item updated."
}
```

### Delete Item
Delete a cart item from an existing cart

**DELETE** : `/cart/item`

| Parameter | Type |Description|
|--|--|----|
| id | int | id of the cart-item|
| user_id | int | the id of user (use `1` for test) |

example request
```
{
    "user_id": 1,
    "id": 6
}
```

**success response:**
```
# status: 200
{
    "result": {
        "cart_item_id": 6,
        "item_id": 5
    },
    "message": "Cart item deleted."
}
```


### Mark As Checked
Marking an item as checked

**PUT** : `cart/item/checked`

| Parameter | Type |Description|
|--|--|----|
| id | int | id of the cart-item|
| user_id | int | the id of user (use `1` for test) |

example request
```
{
    "user_id": 1,
    "id": 1
}
```

**success response:**
```
# status: 200
{
	"result": {
		"cart_item_id": 1
	},
	"message": "Cart item marked as checked."
}
```
