<?php

require_once "vendor/autoload.php";

use App\Database\Connection;
use App\Database\Database;


function add_users(Database $db)
{
    $db->create("users", [
         "firstname" => "test",
         "lastname"  => "test last name",
         "email"     => "test@gmail.com",
    ]);
}


function add_items(Database $db)
{
    $db->create("items", [
         "title"           => "item 1",
         "slug"            => "item-1",
         "price"           => 54.21,
         "inventory_count" => 1000,
    ]);
    $db->create("items", [
         "title"           => "item 2",
         "slug"            => "item-2",
         "price"           => 12.49,
         "inventory_count" => 2000,
    ]);

    $db->create("items", [
         "title"           => "item 3",
         "slug"            => "item-3",
         "price"           => 83.26,
         "inventory_count" => 1000,
    ]);
    $db->create("items", [
         "title"           => "item 4",
         "slug"            => "item-4",
         "price"           => 10.20,
         "inventory_count" => 2000,
    ]);
    $db->create("items", [
         "title"           => "item 5",
         "slug"            => "item-5",
         "price"           => 44.36,
         "inventory_count" => 1000,
    ]);
}


$db = new Database();

add_users($db);
add_items($db);

Connection::close();
