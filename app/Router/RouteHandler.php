<?php

namespace App\Router;

class RouteHandler
{

    public string $method = "";

    public string $url = "";

    public string $controller = "";

    public string $controller_method = "";



    /**
     * @param string $method
     * @param string $url
     * @param string $controller
     * @param string $controller_method
     */
    public function __construct(string $method, string $url, string $controller, string $controller_method)
    {
        $this->method            = $method;
        $this->url               = $url;
        $this->controller        = $controller;
        $this->controller_method = $controller_method;
    }
}
