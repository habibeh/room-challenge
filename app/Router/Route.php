<?php

namespace App\Router;

use App\Library\Container;
use App\Library\Request;
use App\Library\View;

class Route
{
    static $routes = [];



    /**
     * find the registered routes based on the uri and call the handler
     *
     * @return void
     * @throws \Exception
     */
    public static function serve()
    {
        $url = explode("?", $_SERVER["REQUEST_URI"])[0];
        /** @var RouteHandler $route_handler */
        $route_handler = static::$routes[$url][$_SERVER["REQUEST_METHOD"]] ?? null;
        if (!$route_handler) {
            View::render("404.html");
            return;
        }

        $class  = "App\\Controllers\\" . $route_handler->controller;
        $method = $route_handler->controller_method;
        if (class_exists($class)) {

            $post_data  = json_decode(file_get_contents('php://input'), true);
            $controller = Container::getInstance()->resolve($route_handler->controller);
            $request    = (new Request())->setQueryParams($_GET)->setBody(count($_POST) ? $_POST : $post_data);

            if (method_exists($controller, $method)) {
                $controller->$method($request);
                return;
            }
        }

        throw new \Exception("Internal error");
    }



    /**
     * add new route the registered routes
     *
     * @param string $method
     * @param string $url
     * @param        $handler
     *
     * @return void
     * @throws \Exception
     */
    public static function add(string $method, string $url, $handler)
    {
        $handlers = explode("@", $handler);
        if (count($handlers) != 2) {
            throw new \Exception("Invalid Route");
        }

        static::$routes[$url][$method] = new RouteHandler($method, $url, $handlers[0], $handlers[1]);
    }



    /**
     * add an GET http route to the registered routes
     *
     * @param string $url
     * @param        $handler
     *
     * @return void
     * @throws \Exception
     */
    public static function get(string $url, $handler)
    {
        static::add("GET", $url, $handler);
    }



    /**
     * add an POST http route to the registered routes
     *
     * @param string $url
     * @param        $handler
     *
     * @return void
     * @throws \Exception
     */
    public static function post(string $url, $handler)
    {
        static::add("POST", $url, $handler);
    }



    /**
     * add an PUT http route to the registered routes
     *
     * @param string $url
     * @param        $handler
     *
     * @return void
     * @throws \Exception
     */
    public static function put(string $url, $handler)
    {
        static::add("PUT", $url, $handler);
    }



    /**
     * add an DELETE http route to the registered routes
     *
     * @param string $url
     * @param        $handler
     *
     * @return void
     * @throws \Exception
     */
    public static function delete(string $url, $handler)
    {
        static::add("DELETE", $url, $handler);
    }
}
