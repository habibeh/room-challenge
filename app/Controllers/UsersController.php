<?php

namespace App\Controllers;


use App\Library\Request;
use App\Library\Response;
use App\Library\View;
use App\Repositories\UserRepository;

class UsersController extends Controller
{

    public function __construct(public UserRepository $repository)
    {
    }



    /**
     * Get the list of users
     *
     * @param Request $request
     *
     * @return void
     */
    public function list(Request $request)
    {
        $users = $this->repository->all();

        View::render("users.php", ["users" => $users]);
    }



    public function create(Request $request)
    {
        $res = $this->repository->create($request->getBody());

        (new Response(["result" => $res], $res ? 200 : 400))->json();
    }
}
