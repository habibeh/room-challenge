<?php

namespace App\Controllers;


use App\Library\Request;
use App\Library\Response;
use App\Library\View;
use App\Repositories\CartItemRepository;
use App\Repositories\CartRepository;
use App\Repositories\ItemRepository;

class CartsController extends Controller
{

    public function __construct(
         public CartRepository $repository,
         public CartItemRepository $cartItemRepository,
         public ItemRepository $itemRepository,
    ) {
    }



    public function index(Request $request)
    {
        $items = $this->itemRepository->list();
        $cart  = $this->repository->current();

        $added_items = array_map(function ($item) {
            return $item->item_id;
        }, $cart->items);

        View::render("index.php", ["items" => $items, "cart" => $cart, "cart_items" => $added_items]);

    }



    public function list(Request $request)
    {
        View::render("carts.php",["carts"=>$this->repository->list()]);
    }



    /**
     * create cart
     *
     * @param Request $request
     *
     * @return void
     */
    public function create(Request $request)
    {
        if (!$request->required(["items", "user_id"])) {
            (new Response(["message" => "items and user_id are required"], 422))->json();
            return;
        }
        $cart_id = $this->repository->create($request->get("user_id"));

        foreach ($request->get("items") as $item) {
            if (!isset($item["item_id"])) {
                (new Response(["message" => "item_id are required"], 422))->json();
                return;
            }

            $q         = $item["quantity"] ?? 1;
            $itemModel = $this->itemRepository->fetch($item["item_id"]);

            if (!$itemModel->exists() || !$itemModel->hasInventory($q)) {
                (new Response(["message" => "item is not available"], 422))->json();
                return;
            }

            $this->cartItemRepository->create([
                 "cart_id"  => $cart_id,
                 "quantity" => $q,
                 "item_id"  => $item["item_id"],
            ]);

            $this->itemRepository->updateInventoryCount($itemModel->id, $itemModel->inventory_count - $q);
        }

        (new Response([
             "result"  => [
                  "cart_id" => $cart_id,
             ],
             "message" => "Cart created.",
        ]))->json();

    }



    /**
     * Add item to cart
     *
     * @param Request $request
     *
     * @return void
     */
    public function addItem(Request $request)
    {
        if (!$request->required(["user_id", "item_id"])) {
            (new Response(["message" => "cart_id and user_id and item_id are required"], 422))->json();
            return;
        }
        $cart_id   = $this->getCartId($request->get("user_id") ?? 1);
        $q         = $request->get("quantity") ?? 1;
        $item_id   = $request->get("item_id") ?? 0;
        $itemModel = $this->itemRepository->fetch($item_id);

        if ($q <= 0) {
            (new Response(["message" => "invalid quantity"], 422))->json();
            return;
        }

        if (!$itemModel->exists() || !$itemModel->hasInventory($q)) {
            (new Response(["message" => "item is not available"], 422))->json();
            return;
        }

        $cart_item_id = $this->cartItemRepository->create([
             "cart_id"  => $cart_id,
             "quantity" => $q,
             "item_id"  => $item_id,
        ]);

        $this->itemRepository->updateInventoryCount($itemModel->id, $itemModel->inventory_count - $q);
        $this->repository->calculatePrice($cart_id);

        (new Response([
             "result"  => [
                  "cart_item_id" => $cart_item_id,
             ],
             "message" => "Cart item added.",
        ]))->json();
    }



    /**
     * edit an item of cart
     *
     * @param Request $request
     *
     * @return void
     */
    public function editItem(Request $request)
    {
        if (!$request->required(["id", "user_id", "quantity"])) {
            (new Response(["message" => "id and quantity and user_id are required"], 422))->json();
            return;
        }
        $q            = $request->get("quantity");
        $cart_item_id = $request->get("id");

        if ($q == 0) {
            $this->deleteItem($request);
            return;
        }

        $cartItemModel = $this->cartItemRepository->fetch($cart_item_id);
        if (!$cartItemModel->exists()) {
            (new Response(["message" => "item not found"], 404))->json();
            return;
        }

        if ($cartItemModel->cart?->isClosed()) {
            (new Response(["message" => "Cart is closed"], 422))->json();
            return;
        }

        $itemModel = $this->itemRepository->fetch($cartItemModel->item_id);

        if ($cartItemModel->quantity > $q) {
            $new_inventory = $itemModel->inventory_count + ($cartItemModel->quantity - $q);
        } else {
            $new_inventory = $itemModel->inventory_count - ($q - $cartItemModel->quantity);
            if (!$itemModel->exists() || !$itemModel->hasInventory($q - $cartItemModel->quantity)) {
                (new Response(["message" => "item is not available"], 422))->json();
                return;
            }
        }

        $updated = $this->cartItemRepository->update([
             "id"       => $cart_item_id,
             "quantity" => $q,
        ]);

        if (!$updated) {
            (new Response(["message" => "update failed"], 500))->json();
            return;
        }

        $this->itemRepository->updateInventoryCount($itemModel->id, $new_inventory);

        $this->repository->calculatePrice($cartItemModel->cart_id);

        (new Response([
             "result"  => [
                  "cart_item_id"     => $cart_item_id,
                  "updated_quantity" => $q,
             ],
             "message" => "Cart item updated.",
        ]))->json();
    }



    public function deleteItem(Request $request)
    {
        if (!$request->required(["id"])) {
            (new Response(["message" => "item not found"], 404))->json();
            return;
        }
        $cart_item_id = $request->get("id");

        $cartItemModel = $this->cartItemRepository->fetch($cart_item_id);
        if (!$cartItemModel->exists()) {
            (new Response(["message" => "item not found"], 404))->json();
            return;
        }

        $itemModel = $this->itemRepository->fetch($cartItemModel->item_id);

        $deleted = $this->cartItemRepository->delete($request->get("id"));
        if (!$deleted) {
            (new Response(["message" => "delete failed"], 500))->json();
            return;
        }

        $this->itemRepository->updateInventoryCount($itemModel->id,
             $itemModel->inventory_count + $cartItemModel->quantity);

        $this->repository->calculatePrice($cartItemModel->cart_id);

        (new Response([
             "result"  => [
                  "item_id"      => $itemModel->id,
                  "cart_item_id" => $cart_item_id,
             ],
             "message" => "Cart item deleted.",
        ]))->json();

    }



    public function markAsChecked(Request $request)
    {
        if (!$request->required(["id"])) {
            (new Response(["message" => "item not found"], 404))->json();
            return;
        }
        $cart_item_id = $request->get("id");

        $cartItemModel = $this->cartItemRepository->fetch($cart_item_id);
        if (!$cartItemModel->exists()) {
            (new Response(["message" => "item not found"], 404))->json();
            return;
        }

        $checked = $this->cartItemRepository->markAsChecked($cart_item_id);
        if (!$checked) {
            (new Response(["message" => "mark as check failed"], 500))->json();
            return;
        }

        (new Response([
             "result"  => [
                  "cart_item_id" => $cart_item_id,
             ],
             "message" => "Cart item marked as checked.",
        ]))->json();

    }



    public function finalize(Request $request)
    {
        $cart = $this->repository->current();
        if (!$cart->exists()) {
            (new Response(["message" => "Cart is closed"], 422))->json();
            return;
        }

        $closed = $this->repository->finalize($cart->id);
        if (!$closed) {
            (new Response(["message" => "finalizing cart failed"], 500))->json();
            return;
        }

        (new Response([
             "result"  => [
                  "cart_id" => $cart->id,
             ],
             "message" => "Cart finalized",
        ]))->json();
    }



    public function fetch(Request $request)
    {
        if (!$request->required(["id"])) {
            (new Response(["message" => "cart not found"], 404))->json();
            return;
        }

        $cart = $this->repository->fetch($request->get("id"));
        if (!$cart->exists()) {
            (new Response(["message" => "cart not found"], 404))->json();
            return;
        }

        $items = $this->cartItemRepository->getArrayByCartId($cart->id);

        (new Response([
             "result" => [
                  "id"    => $cart->id,
                  "items" => $items,
             ],
        ]))->json();
    }



    private function getCartId(int $user_id)
    {
        $cart_id = $this->repository->current()->id;
        if ($cart_id == 0) {
            $cart_id = $this->repository->create($user_id);
        }

        return $cart_id;
    }
}
