<?php

namespace App\Models;

class CartItem extends Model
{
    public int       $cart_id  = 0;

    public int       $item_id  = 0;

    public int       $quantity = 0;

    public bool      $checked  = false;

    public float     $price    = 0;

    /**
     * @var Cart|null
     */
    public $cart;

}
