<?php

namespace App\Models;

class Item extends Model
{
    public string $title;

    public string $slug;

    public float $price = 0;

    public int    $inventory_count = 0;



    public function hasInventory($quantity)
    {
        return $this->inventory_count >= $quantity;
    }

}
