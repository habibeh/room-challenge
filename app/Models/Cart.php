<?php

namespace App\Models;

class Cart extends Model
{
    public int   $user_id     = 0;

    public float $total_price = 0;

    public array $items       = [];

    public bool  $closed    = false;



    public function isClosed()
    {
        return $this->closed;
    }
}
