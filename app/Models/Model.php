<?php

namespace App\Models;

#[AllowDynamicProperties]
class Model
{
    public $id = 0;



    /**
     * bind a collection of associated array to the models
     *
     * @param array $data
     *
     * @return array
     */
    public static function bindAll($data): array
    {
        $models = [];
        foreach ($data as $d) {
            $model = new static();
            foreach ($d as $item => $value) {
                $model->$item = $value ?? "";
            }
            $models[] = $model;
        }

        return $models;
    }



    /**
     * bind anassociated array to the models
     *
     * @param array $data
     *
     * @return static
     */
    public static function bind($data): static
    {
        $model = new static();
        foreach ($data as $item => $value) {
            $model->$item = $value;
        }
        return $model;
    }



    public function exists(): bool
    {
        return $this->id != 0;
    }
}
