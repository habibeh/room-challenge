<?php

namespace App\Models;

class User extends Model
{
    public string $firstname = "";

    public string $lastname  = "";

    public string $email     = "";

    public string $reg_date  = "";
}
