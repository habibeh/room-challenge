<?php

namespace App\Library;

class Request
{
    private $params = [];

    private $body   = [];



    /**
     * set the query parameters
     *
     * @param array $params
     *
     * @return $this
     */
    public function setQueryParams($params): static
    {
        $this->params = $params;

        return $this;
    }



    /**
     * get the params array
     *
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }



    /**
     * get a param by key or return the whole array instead
     *
     * @param $key
     *
     * @return array|mixed|null
     */
    public function get($key)
    {
        return $this->params[$key] ?? $this->body[$key] ?? null;
    }



    /**
     * get a body by key or return the whole array instead
     *
     * @param $key
     *
     * @return array|mixed|null
     */
    public function getBody()
    {
        return $this->body;
    }



    /**
     * set body array
     *
     * @param array $body
     *
     * @return $this
     */
    public function setBody($body): static
    {
        $this->body = $body;
        return $this;
    }



    public function required($items): bool
    {
        foreach ($items as $item) {
            if ($this->get($item) === null) {
                return false;
            }
        }

        return true;
    }
}
