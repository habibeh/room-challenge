<?php

namespace App\Library;


class View
{
    /**
     * render a view with the variables
     *
     * @param $path
     * @param $data
     *
     * @return void
     */
    public static function render($path, $data = [])
    {
        foreach ($data as $name => $value) {
            $$name = $value;
        }
        require_once str_replace("Library", "", __DIR__) . "views/" . $path;
    }

}
