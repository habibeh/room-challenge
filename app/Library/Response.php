<?php

namespace App\Library;

class Response
{
    private $data;

    private $statusCode;

    private $headers = [];



    public function __construct($data = [], $statusCode = 200)
    {
        $this->data       = $data;
        $this->statusCode = $statusCode;
    }



    /**
     * set headers array
     *
     * @param string $name
     * @param string $value
     *
     * @return void
     */
    public function setHeader($name, $value)
    {
        $this->headers[$name] = $value;
    }



    /**
     * Create json response
     *
     * @return void
     */
    public function json()
    {
        header('Content-Type: application/json');
        http_response_code($this->statusCode);

        foreach ($this->headers as $name => $value) {
            header("$name: $value");
        }

        echo json_encode($this->data);
    }
}

