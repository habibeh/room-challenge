<?php

namespace App\Library;

use Exception;
use ReflectionClass;

class Container
{
    /**
     * @var static|null
     */
    private static $instance;

    private        $bindings = [];



    protected function __construct()
    {
    }



    public static function getInstance(): static
    {
        if (static::$instance) {
            return static::$instance;
        }

        return static::$instance = new static();
    }



    public function bind($abstract, $concrete = null)
    {
        if ($concrete === null) {
            $concrete = $abstract;
        }
        $this->bindings[$abstract] = $concrete;
    }



    public function resolve($abstract)
    {
        if (isset($this->bindings[$abstract])) {
            $concrete = $this->bindings[$abstract];
            if (is_callable($concrete)) {
                return $concrete($this);
            } else {
                return $this->build($concrete);
            }
        }
        throw new Exception("Unresolved dependency: $abstract");
    }



    public function build($concrete)
    {
        $reflector   = new ReflectionClass($concrete);
        $constructor = $reflector->getConstructor();

        if ($constructor === null) {
            return $reflector->newInstance();
        }

        $dependencies = $this->getDependencies($constructor->getParameters());

        return $reflector->newInstanceArgs($dependencies);
    }



    public function getDependencies($parameters)
    {
        $dependencies = [];
        foreach ($parameters as $parameter) {
            if ($parameter->getClass() !== null) {
                $dependencies[] = $this->resolve($parameter->getClass()->name);
            } else {
                if ($parameter->isDefaultValueAvailable()) {
                    $dependencies[] = $parameter->getDefaultValue();
                } else {
                    throw new Exception("Unable to resolve a dependency for {$parameter->name}");
                }
            }
        }
        return $dependencies;
    }
}

