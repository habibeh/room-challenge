<?php

namespace App\Database;

use PDO;
use PDOException;

class Database
{
    private      $conn;

    private bool $executed;



    public function __construct()
    {
        $this->conn = Connection::open();
    }



    /**
     * fetch all rows as an associate array
     *
     * @param string     $query
     * @param array|null $bindings
     *
     * @return array|false
     */
    public function all(string $query, ?array $bindings = [])
    {
        return $this->_exec($query, $bindings)->fetchAll(PDO::FETCH_ASSOC);
    }



    /**
     * fetch single row as an associate array
     *
     * @param string     $query
     * @param array|null $bindings
     *
     * @return mixed
     */
    public function fetch(string $query, ?array $bindings = [])
    {
        return $this->_exec($query, $bindings)->fetch(PDO::FETCH_ASSOC);
    }



    /**
     * insert a record to database and return effected result
     *
     * @param $table
     * @param $data
     *
     * @return int
     */
    public function create($table, $data)
    {
        $params = [];
        $values = [];
        foreach ($data as $k => $v) {
            $params[] = $k;
            $values[] = ":" . $k;
        }
        $params = implode(", ", $params);
        $values = implode(", ", $values);

        $this->execute("INSERT INTO $table ($params) VALUES ($values)", $data);

        return $this->conn->lastInsertId();
    }



    /**
     * update a record to database and return effected result
     *
     * @param $table
     * @param $data
     *
     * @return bool
     */
    public function update($table, $data, $field = "id")
    {
        $params = [];
        foreach ($data as $k => $v) {
            $params[] = $k . " = " . ":" . $k;
        }
        $params = implode(", ", $params);

        $sql = "UPDATE $table SET $params WHERE $field = :$field";
        return $this->execute($sql, $data);
    }



    /**
     * execute the query with the bindings and return the effected result
     *
     * @param string     $query
     * @param array|null $bindings
     *
     * @return bool
     * @throws PDOException
     */
    public function execute(string $query, ?array $bindings = [])
    {
        $this->_exec($query, $bindings);

        return $this->executed;
    }



    /**
     * execute the query and return the statement
     *
     * @param string     $query
     * @param array|null $bindings
     *
     * @return false|\PDOStatement
     */
    private function _exec(string $query, ?array $bindings = [])
    {
        $stmt = $this->conn->prepare($query);

        foreach ($bindings as $key => $value) {
            $stmt->bindValue(":" . $key, $value);

            //$stmt->bindParam(":" . $key, $value);
        }

        $this->executed = $stmt->execute();

        return $stmt;
    }



    public function delete(string $string, $data)
    {
    }
}
