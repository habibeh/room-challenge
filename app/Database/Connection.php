<?php

namespace App\Database;

use PDO;

class Connection
{
    private static array $options = [
         PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
         PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    ];

    protected static     $conn;



    /**
     * Open a PDO connection
     *
     * @return PDO
     */
    public static function open(): PDO
    {
        if (static::$conn) {
            return static::$conn;
        }
        return static::$conn = new PDO("mysql:host=" . getenv("MYSQL_HOST") . ":3306;dbname=" . getenv("MYSQL_DATABASE"),
             getenv("MYSQL_USER"), getenv("MYSQL_PASSWORD"), static::$options);
    }



    /**
     * close the PDO connection
     *
     * @return void
     */
    public static function close()
    {
        static::$conn = null;
    }
}
