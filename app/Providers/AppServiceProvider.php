<?php

namespace App\Providers;

use App\Controllers\CartsController;
use App\Controllers\UsersController;
use App\Database\Database;
use App\Library\Container;
use App\Repositories\Mysql\CartItemRepository;
use App\Repositories\Mysql\CartRepository;
use App\Repositories\Mysql\ItemRepository;
use App\Repositories\Mysql\UserRepository;

class AppServiceProvider
{
    /**
     * register application
     *
     * @return Container
     */
    public function register(): Container
    {
        $container = Container::getInstance();

        $container->bind("UserRepository", function ($container) {
            return new UserRepository(new Database());
        });
        $container->bind("UsersController", function ($container) {
            return new UsersController($container->resolve("UserRepository"));
        });

        $container->bind("CartRepository", function ($container) {
            return new CartRepository(new Database());
        });

        $container->bind("CartItemRepository", function ($container) {
            return new CartItemRepository(new Database());
        });
        $container->bind("ItemRepository", function ($container) {
            return new ItemRepository(new Database());
        });
        $container->bind("CartsController", function ($container) {
            return new CartsController(
                 $container->resolve("CartRepository"),
                 $container->resolve("CartItemRepository"),
                 $container->resolve("ItemRepository"),
            );
        });

        return $container;
    }
}
