<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Orders List</title>
	<link rel="stylesheet" href="app/views/assets/styles.css">
</head>
<body>

<nav>
	<a href="/" <?= $_SERVER["REQUEST_URI"] == "/" ? "class='active'" : ""?>>Shopping Cart</a>
	<a href="/carts" <?= $_SERVER["REQUEST_URI"] == "/carts" ? "class='active'" : ""?>>Orders List</a>
</nav>
<h2>Orders List</h2>

<table>
	<tr>
		<th>ID</th>
		<th>Total Price</th>
		<th>Items</th>
		<th>Created Date</th>
	</tr>

    <?php foreach ($carts as $order): ?>
		<tr>
			<td><?= $order->id ?></td>
			<td><?= $order->total_price ?></td>
			<td>
                <?php foreach ($order->items as $item): ?>
                    <?= $item->title ?> (Qty: <?= $item->quantity ?>)<br>
                <?php endforeach; ?>
			</td>
			<td><?= $order->created_at ?></td>
		</tr>
    <?php endforeach; ?>

</table>

</body>
</html>
