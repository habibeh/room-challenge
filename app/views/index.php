<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="app/views/assets/styles.css">
</head>
<body>


<nav>
	<a href="/" <?= $_SERVER["REQUEST_URI"] == "/" ? "class='active'" : "" ?>>Shopping Cart</a>
	<a href="/carts" <?= $_SERVER["REQUEST_URI"] == "/carts" ? "class='active'" : "" ?>>Orders List</a>
</nav>

<h2>Items: </h2>

<?php foreach ($items as $item) { ?>
	<div class="shop-item">
		<span>Name: <?= $item->title ?></span>
		<span>Price: $<?= $item->price ?></span>
		<input type="number" id="<?= $item->slug ?>" value="1" min="1" max="10">
		<button
             <?php if (in_array($item->id, $cart_items)) { ?>
				 style="cursor: default; background-color: gray"
				 disabled
             <?php } ?>
				onclick="addItem('<?= $item->id ?>','<?= $item->slug ?>')">
			Add to Cart
		</button>
	</div>
<?php } ?>


<?php if ($cart->id != 0) { ?>
	<div class="cart">
		<h2>Shopping Cart</h2>
		<ul id="cart-items">
            <?php foreach ($cart->items as $item) { ?>
				<li>
					<div class="cart-item">
						<span><?= $item->title ?> x<?= $item->quantity ?> - $<?= $item->price * $item->quantity ?></span>
						<div class="quantity-buttons">
							<button onclick="editItem('<?= $item->id ?>','<?= $item->quantity ?>', 'add')">+</button>
							<button onclick="editItem('<?= $item->id ?>','<?= $item->quantity ?>', 'remove')">-</button>
							<button style="background-color: #ff5555" onclick="deleteItem('<?= $item->id ?>')">
								&#128465;
							</button>
						</div>
					</div>
				</li>
            <?php } ?>
		</ul>
		<h3>Total: $<span id="cart-total"><?= $cart->total_price ?></span></h3>

		<button id="finalize" onclick="finalize('<?= $cart->id ?>')">Finalize</button>
	</div>
<?php } else { ?>
	<div class="cart">
		<h2>Shopping Cart</h2>
		<ul id="cart-items">
		</ul>
		<h3>Total: $<span id="cart-total">0.00</span></h3>
	</div>
<?php } ?>
<?php $base_url = getenv('BASE_URL') ?>
<script>
    let cart = [];

    function addItem(id, inputId) {
        const quantity = parseInt(document.getElementById(inputId).value);
        const itemData = {
            user_id : 1,
            item_id : id,
            quantity: quantity,
        }

        fetch("http://<?=$base_url ?>:8001/cart/item", {
            method : 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body   : JSON.stringify(itemData)
        }).then(setTimeout(location.reload.bind(location), 50))
    }

    function editItem(id, quantity, typ) {
        if (typ === "add") {
            quantity++
        } else {
            quantity--
        }
        const itemData = {
            id      : id,
            user_id : 1,
            quantity: quantity,
        }
        fetch("http://<?=$base_url ?>:8001/cart/item", {
            method : 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body   : JSON.stringify(itemData)
        }).then(setTimeout(location.reload.bind(location), 50))
    }

    function deleteItem(id) {
        const itemData = {
            id: id,
        }
        fetch("http://<?=$base_url ?>:8001/cart/item", {
            method : 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            },
            body   : JSON.stringify(itemData)
        }).then(setTimeout(location.reload.bind(location), 50))
    }

    function finalize(id) {
        const cartData = {
            id: id,
        }
        fetch("http://<?=$base_url ?>:8001/cart/finalize", {
            method : 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body   : JSON.stringify(cartData)
        }).then(setTimeout(location.reload.bind(location), 50))
    }
</script>
</body>
</html>
