<?php

require_once "vendor/autoload.php";

(new \App\Providers\AppServiceProvider())->register();
