<?php

namespace App\Repositories\Mysql;

use App\Models\Item;
use App\Repositories\Repository;

class ItemRepository extends Repository implements \App\Repositories\ItemRepository
{
    public function fetch($id): Item
    {
        $data = $this->db->fetch("SELECT * FROM items WHERE id = :id", [
             "id" => $id,
        ]);

        return Item::bind($data);
    }



    public function updateInventoryCount($id, $inventory_count)
    {
        $this->db->update('items', [
             "id"              => $id,
             "inventory_count" => $inventory_count,
        ]);
    }



    public function list(): array
    {
        return Item::bindAll($this->db->all("SELECT * FROM items"));
    }
}
