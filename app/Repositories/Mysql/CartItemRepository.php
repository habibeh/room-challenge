<?php

namespace App\Repositories\Mysql;

use App\Models\Cart;
use App\Models\CartItem;
use App\Repositories\Repository;

class CartItemRepository extends Repository implements \App\Repositories\CartItemRepository
{
    public function create(array $data = [])
    {
        return $this->db->create("cart_items", $data);
    }



    public function fetch(int $id): CartItem
    {
        $data = $this->db->fetch("SELECT * FROM cart_items WHERE id = :id", [
             "id" => $id,
        ]);

        $cartItem = CartItem::bind($data);

        $cartItem->cart = Cart::bind($this->db->fetch("SELECt * FROM carts WHERE id = :id", [
             "id" => $cartItem->cart_id,
        ]));

        return $cartItem;
    }



    public function update(array $data = [])
    {
        return $this->db->update("cart_items", $data);
    }



    public function delete(int $id)
    {
        return $this->db->execute("DELETE FROM cart_items WHERE id = :id", [
             "id" => $id,
        ]);
    }



    public function getArrayByCartId(int $cart_id)
    {
        return $this->db->all("SELECT * FROM cart_items WHERE cart_id = :cart_id", [
             "cart_id" => $cart_id,
        ]);
    }



    public function markAsChecked($id): bool
    {
        return $this->db->update("cart_items", [
             "id"      => $id,
             "checked" => true,
        ]);
    }

}
