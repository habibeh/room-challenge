<?php

namespace App\Repositories\Mysql;

use App\Models\User;
use App\Repositories\Repository;
use PDOException;

class UserRepository extends Repository implements \App\Repositories\UserRepository
{

    public function all()
    {
        $res = $this->db->all("SELECT * FROM users");

        return User::bindAll($res);
    }



    public function create($data = [])
    {
        return $this->db->create("users", $data);

        try {
            return $this->db->create("users", $data);
        } catch (PDOException $exception) {
            return false;
        }
    }
}
