<?php

namespace App\Repositories\Mysql;

use App\Models\Cart;
use App\Models\CartItem;
use App\Models\User;
use App\Repositories\Repository;
use PDOException;

class CartRepository extends Repository implements \App\Repositories\CartRepository
{
    public function create($user_id)
    {
        return $this->db->create("carts", [
             "user_id" => $user_id,
        ]);
    }



    public function fetch($id): Cart
    {
        $cart = $this->db->fetch("SELECT * FROM carts WHERE id = :id", [
             "id" => $id,
        ]);

        return Cart::bind($cart);
    }



    public function current(): Cart
    {
        $row  = $this->db->fetch("SELECT * FROM carts WHERE closed = 0 limit 1");
        $cart = Cart::bind($row);

        $cart->items = CartItem::bindAll($this->db->all("SELECT ci.id, ci.quantity, ci.item_id, items.price, items.title FROM cart_items ci JOIN items on items.id = item_id WHERE cart_id = :cart_id",
             [
                  "cart_id" => $cart->id,
             ]));

        return $cart;
    }



    public function calculatePrice(int $id)
    {
        $query = "UPDATE carts
SET total_price = COALESCE(
    (
        SELECT SUM(cart_items.quantity * items.price)
        FROM cart_items
        JOIN items ON items.id = item_id
        WHERE cart_items.cart_id = :id
    ), 0)
WHERE carts.id = :cart_id;";

        $this->db->execute($query, [
             "id"      => $id,
             "cart_id" => $id,
        ]);
    }



    public function finalize(int $id)
    {
        return $this->db->update("carts", [
             "id"     => $id,
             "closed" => true,
        ]);
    }



    public function list(): array
    {
        $result = [];
        $rows   = $this->db->all("SELECT * FROM carts order by created_at desc");
        foreach ($rows as $row) {
            $cart = Cart::bind($row);

            $cart->items = CartItem::bindAll($this->db->all("SELECT ci.id, ci.quantity, ci.item_id, items.price, items.title FROM cart_items ci JOIN items on items.id = item_id WHERE cart_id = :cart_id",
                 [
                      "cart_id" => $cart->id,
                 ]));

            $result[] = $cart;
        }

        return $result;
    }
}
