<?php

namespace App\Repositories;

use App\Models\Item;

interface ItemRepository
{
    public function fetch($id): Item;



    public function updateInventoryCount($id, $inventory_count);



    public function list(): array;
}
