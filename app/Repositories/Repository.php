<?php

namespace App\Repositories;

use App\Database\Database;

class Repository
{
    public function __construct(protected Database $db)
    {
    }
}
