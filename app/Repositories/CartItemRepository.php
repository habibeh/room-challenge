<?php

namespace App\Repositories;

use App\Models\CartItem;

interface CartItemRepository
{

    public function create(array $data = []);



    public function fetch(int $id): CartItem;



    public function update(array $array = []);



    public function delete(int $id);



    public function getArrayByCartId(int $cart_id);



    public function markAsChecked(int $id);
}
