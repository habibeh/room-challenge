<?php

namespace App\Repositories;

use App\Models\Cart;

interface CartRepository
{
    public function create($user_id);



    public function fetch(int $id): Cart;



    public function current(): Cart;



    public function calculatePrice(int $id);



    public function finalize(int $id);



    public function list(): array;


}
