<?php

\App\Router\Route::get("/", "CartsController@index");
\App\Router\Route::get("/carts", "CartsController@list");

\App\Router\Route::get("/users", "UsersController@list");
\App\Router\Route::post("/user", "UsersController@create");

\App\Router\Route::post("/cart", "CartsController@create");
\App\Router\Route::get("/cart", "CartsController@fetch");
\App\Router\Route::put("/cart/finalize", "CartsController@finalize");

\App\Router\Route::post("/cart/item", "CartsController@addItem");
\App\Router\Route::put("/cart/item", "CartsController@editItem");
\App\Router\Route::delete("/cart/item", "CartsController@deleteItem");
\App\Router\Route::put("/cart/item/checked", "CartsController@markAsChecked");

